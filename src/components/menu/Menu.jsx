import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { FaTimes } from 'react-icons/fa';
import './menu.css';

function Menu({ navItems }) {

	const links = navItems;
	const [toggleMenu, setToggleMenu] = useState(false);

	const handleClick = () => {
		setToggleMenu(!toggleMenu);
	}

	return (
		<ul className='menu' onClick={handleClick}>
			{
				links.map(link => (
					<Link
						key={link.id}
						to={link.path}
						className={toggleMenu === false ? 'menu-link' : `menu-link ${link.linkOpen}`}
					>
						{link.icon}
					</Link>
				))
			}
			<li className={toggleMenu === false ? 'menu-btn' : 'menu-btn menu-btn-open'}>
				<FaTimes />
			</li>
		</ul>
	)
}

export default Menu