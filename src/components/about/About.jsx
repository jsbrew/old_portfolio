import React from 'react';
import handsome from '../../assets/handsome.png';
import './about.css';

function About({ contacts }) {

	const contactMethods = contacts;

	return (
		<section className='about-section'>
			<div className='about-info'>
				<div className="profile-img">
					<img className='profile' alt='handsome-profile' src={handsome} />
				</div>
				<div className="profile-text">
					<div className='section-headers'>
						<h5>Get to know</h5>
						<h2>About <span className='highlight-word'>Me</span></h2>
					</div>
					<div className='section-text'>
						<p>
							Welcome to my portfolio, my name is Juuso Seppälä. I am a second-year student at Oulu University of Applied Sciences 
							and currently I am pursuing a Bachelor's Degree in Business Information Technology. 
							At the moment I am residing in Tampere and conducting my studies remotely, but that is the beauty of the present day.
						</p>
						<p>
							I have more than ten years of experience in the security field, mainly working in traffic stations and hospitals. 
							I feel that my work has taught me how to work under high pressure, and how to work with different 
							kinds of people.
						</p>
						<p>
							Also, feel free to contact me for further collaboration or check out my GitHub.
						</p>
					</div>
					<div className="profile-contact">
						{
							contactMethods.map(contact => (
								<a 
									key={contact.id} 
									href={contact.href} 
									target={contact.id === 2 ? '_blank' : ''}
									rel='noopener noreferrer'
								>
									{contact.icon}
								</a>
							))
						}
					</div>
				</div>
			</div>
		</section>
	)
}

export default About