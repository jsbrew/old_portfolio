import React from 'react';
import { FaCog } from 'react-icons/fa';
import './header.css';

function Header() {
	return (
		<div className='header'>
			<div className="logo-bg">
				<FaCog />
			</div>
			<div className="header-text">
				<h1>JSCraft</h1>
				<p>A front-end developer crafting aesthetic and scalable solutions</p>
			</div>
		</div>
	)
}

export default Header