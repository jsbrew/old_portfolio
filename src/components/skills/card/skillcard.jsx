import React from 'react';
import { MdAdsClick } from 'react-icons/md';
import './skillcard.css';

function Skillcard({ cardData }) {
	return (
		<div className='skill-card'>
			<div className="skill-card-icon">
				{cardData.icon}
				<span className='skill-hover'><MdAdsClick /></span>
			</div>
			<div className="skill-card-content">
				<h3>{cardData.title}</h3>
				<p>{cardData.lvl}</p>			
			</div>
		</div>
	)
}

export default Skillcard