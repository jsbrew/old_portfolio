import React from 'react';
import Skillcard from './card/skillcard.jsx';
import './skills.css';

function Skills({ skills }) {

	const allSkills = skills;

	return (
		<section className='skills-section'>
			<div className='section-headers'>
				<h5>Technologies that I'm familiar with</h5>
				<h2>My <span className='highlight-word'>Experience</span></h2>
			</div>
			<div className="skills-content">
				<div className="focus-info">
					<div className="web-dev section-text">
						<h3>Web Development</h3>
						<p>
							Mainly my studies have been based on web development, 
							where I have been introduced to wide range of technologies through various 
							small and medium sized projects.
						</p>
					</div>
					<div className="cyber section-text">
						<h3>Cyber Security</h3>
						<p>
							A new field in which I have a keen interest. Currently I am improving skills, such as pentesting, 
							at mooc.fi, tryhackme.com and PortSwigger's Web Security Academy, among others.
						</p>
					</div>
				</div>
				<div className="skill-cards">
					{
						allSkills.map(skill => (
							<Skillcard key={skill.id} cardData={skill} />
						))
					}
				</div>
			</div>
		</section>
	)
}

export default Skills