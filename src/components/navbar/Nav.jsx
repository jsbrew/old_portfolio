import React from 'react';
import { Link } from 'react-router-dom';
import { FaCog } from 'react-icons/fa';
import './nav.css';

function Nav({ navItems }) {

	const links = navItems;

	return (
		<nav className='navbar'>
			<div className="nav-logo">
				<div className="nav-cog">
					<FaCog />
				</div>
				<h2>JSCraft</h2>
			</div>
			<ul className='nav-links'>
				{
					links.map(link => (
						<Link
							key={link.id}
							to={link.path}
							className='nav-link'
						>
							{link.title}
						</Link>
					))
				}
			</ul>
		</nav>
	)
}

export default Nav