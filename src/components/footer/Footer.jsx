import React from 'react';
import { FaHeart, FaReact, FaPlus } from 'react-icons/fa';
import './footer.css';

function Footer({ contacts, year }) {

	const contactMethods = contacts;
	const currentYear = year;

	return (
		<footer className='footer'>
			<div className="footer-top">
				<div className='footer-summary'>
					<h2>Juuso Seppälä</h2>
					<p>
						A front-end-focused web developer building websites and web applications with care and competence
					</p>
				</div>
				<div className='footer-made'>
					<h2>Made from scratch with</h2>
					<div className='footer-made-icons'>
						<FaHeart />
						<FaPlus />
						<FaReact />
					</div>
				</div>
				<div className='footer-contact'>
					<h2>Let's Collaborate</h2>
					<div className='footer-links'>
						{
							contactMethods.map(contact => (
								<a 
									key={contact.id} 
									href={contact.href} 
									target={contact.id === 2 ? '_blank' : ''}
									rel={contact.rel}
								>
									{contact.icon}
								</a>
							))
						}
					</div>
				</div>
			</div>
			<div className="footer-bottom">
				<h5>&copy; {currentYear} | Made by Juuso Seppälä</h5>
			</div>
		</footer>
	)
}

export default Footer