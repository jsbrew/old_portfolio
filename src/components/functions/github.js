const githubData = async () => {

	const gitData = [];

	await fetch('https://api.github.com/users/juse-dev/repos')
	.then(response => response.json())
	.then(data => {

		data.map(item => gitData.push(
		{
			id: item.id,
			name: item.name,
			desc: item.description,
			url: item.html_url
		}
		))
	})
	console.log(gitData);
	
	return(gitData)
}

export default githubData