import Skills from '../components/skills/Skills.jsx';
import Projects from '../components/projects/Projects.jsx';
import Education from '../components/education/Education.jsx';
import Interests from '../components/interests/Interests.jsx';
import {FaBook, FaCode, FaLightbulb, FaHeart } from 'react-icons/fa';

const navItems = [
	{
		id: 1,
		path: '/',
		component: <Skills />,
		title: 'Skills',
		icon: <FaBook />,
		linkOpen: 'link1-open',
	},
	{
		id: 2,
		path: '/projects',
		component: <Projects />,
		title: 'Projects',
		icon: <FaCode />,
		linkOpen: 'link2-open',
	},
	{
		id: 3,
		path: '/education',
		component: <Education />,
		title: 'Education',
		icon: <FaLightbulb />,
		linkOpen: 'link3-open',
	},
	{
		id: 4,
		path: '/interests',
		component: <Interests />,
		title: 'Interests',
		icon: <FaHeart />,
		linkOpen: 'link4-open',
	}
]

export default navItems