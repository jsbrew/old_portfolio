import { FaHtml5, FaCss3Alt, FaReact, FaBootstrap, FaPython, FaGitSquare, FaWordpress, FaLinux, FaCss3 } from 'react-icons/fa';
import { SiJavascript } from 'react-icons/si';

const skillItems = [
	{
		id: 1,
		title: 'HTML',
		lvl: 'Intermediate',
		icon: <FaHtml5 />,
	},
	{
		id: 2,
		title: 'CSS',
		lvl: 'Intermediate',
		icon: <FaCss3 />,
	},
	{
		id: 3,
		title: 'JavaScript',
		lvl: 'Intermediate',
		icon: <SiJavascript />,
	},
	{
		id: 4,
		title: 'React',
		lvl: 'Intermediate',
		icon: <FaReact />,
	},
	{
		id: 5,
		title: 'Bootstrap',
		lvl: 'Intermediate',
		icon: <FaBootstrap />,
	},
	{
		id: 6,
		title: 'Python',
		lvl: 'Basic',
		icon: <FaPython />,
	},
	{
		id: 7,
		title: 'WordPress',
		lvl: 'Basic',
		icon: <FaWordpress />,
	},
	{
		id: 8,
		title: 'Git',
		lvl: 'Basic',
		icon: <FaGitSquare />,
	},
	{
		id: 9,
		title: 'Linux',
		lvl: 'Intermediate',
		icon: <FaLinux />,
	},
]

export default skillItems