import { FaEnvelope, FaGithub } from 'react-icons/fa';

const contactItems = [
	{
		id: 1,
		href: 'mailto:jsdev@tuta.io',
		icon: <FaEnvelope />,
	},
	{
		id: 2,
		href: 'https://github.com/JuSe-dev?tab=repositories',
		icon: <FaGithub />,
	},
]

export default contactItems