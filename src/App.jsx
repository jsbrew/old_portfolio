import React from 'react';
import { HashRouter, Routes, Route } from 'react-router-dom';
import Nav from './components/navbar/Nav.jsx';
import Menu from './components/menu/Menu.jsx';
import Header from './components/header/Header.jsx';
import Footer from './components/footer/Footer.jsx';
import About from './components/about/About.jsx';
import Skills from './components/skills/Skills.jsx';
import Projects from './components/projects/Projects.jsx';
import Education from './components/education/Education.jsx';
import Interests from './components/interests/Interests.jsx';
import navItems from './data/navitems.js';
import contactItems from './data/contactitems';
import skillItems from './data/skillitems.js';
import setYear from './components/functions/year.js';

function App() {

	const skills = skillItems;
	const contacts = contactItems;
	const year = setYear();

	return (
		<HashRouter>
			<div className="content-wrapper">
				<Header />
				<div className='main-container'>
					<Nav navItems={navItems} />
					<div className="info-container">
						<About contacts={contacts} />
						<Routes>
							<Route path='/' element={<Skills skills={skills} />} />
							<Route path='/projects' element={<Projects />} />
							<Route path='/education' element={<Education />} />
							<Route path='/interests' element={<Interests />} />
						</Routes>
					</div>
				</div>
				<Footer contacts={contacts} year={year} />
			</div>
			<Menu navItems={navItems} />
		</HashRouter>
	)
}

export default App